jQuery(function($){
	$(document).ready(function(){



		$(document).on('click','a[href$="png"],a[href$="jpg"],a[href$="jpeg"],a[href$="gif"]',function(e){
			e.preventDefault();

			var getUrl = $(this).attr('href');

			var beforeSentTem = '<div id="slb_viewer_wrap"><div id="slb_viewer_slb_default" class="slb_viewer slb_theme_slb_default slb_theme_slb_baseline item_single" style="display: block;"> <div class="slb_viewer_layout" style="display: block;"> <div class="slb_container"> <div class="slb_content" style="width: 400px; height: 426px;"><span class="slb_template_tag slb_template_tag_item slb_template_tag_item_content" style="display: inline;"></span> <div class="slb_nav"><span class="slb_prev"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_prev" style="display: block;">Previous</span></span><span class="slb_next"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_next" style="display: block;">Next</span></span> </div><div class="slb_controls"><span class="slb_close"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_close" style="display: block;">Close</span></span><span class="slb_slideshow"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_slideshow_control" style="display: block;">Start slideshow</span></span> </div><div class="slb_loading" style="display:inline-block"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_loading" style="display: inline;">Loading</span></div></div></div></div><div class="slb_viewer_overlay" style="display: block; opacity: 0.8;"></div></div></div>';

    		$.ajax({
    			type: 'post',
    			url: wp_ajax_params.wpsl_ajax_url,
    			data: {
    				action: 'wpsl_ajax_action',
    				wpsl_ajax_nonce: wp_ajax_params.wpsl_ajax_nonce,
    				geturl: getUrl,
    			},
    			beforeSend: function(data){
    				
    				$('.wpsl-container').fadeOut(300, function() {
						$(this).html(beforeSentTem).fadeIn(300);						
					});
    			},
    			complete: function(data){
    				$('.wpsl-container').find('.slb_loading').hide();
    			},    			
    			success: function(data){
    				$('.wpsl-container').html(data);
    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

			console.log(getUrl);
		});

		$(document).on('click','.slb_template_tag_ui_close',function(){			
			$('#slb_viewer_wrap').fadeOut('slow');			
		});
		
	})
})