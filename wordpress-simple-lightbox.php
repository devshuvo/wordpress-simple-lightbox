<?php
/**
 * Plugin Name: WordPress Simple Lightbox
 * Plugin URI: https://devshuvo/plugins/wordpress-simple-lightbox
 * Bitbucket Plugin URI: https://bitbucket.org/devshuvo/wordpress-simple-lightbox/
 * Description: WordPress Simple Lightbox WordPress 5x Compatable 
 * Author: Asrcoder
 * Text Domain: wpsl
 * Domain Path: /lang/
 * Author URI: https://devshuvo.com
 * Tags: wordpress
 * Version: 1.0.0
 */

define( 'WPSL_PLUGIN_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

define( 'WPSL_PLUGIN_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );


// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
* Including Plugin file for security
* Include_once
* 
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 *	Enqueue wooslidebox scripts
 *
 */
function wpsl_enqueue_scripts(){

	wp_enqueue_style('wpsl-styles', plugin_dir_url( __FILE__ ).'css/wpsl-style.css' );

	wp_enqueue_script('wpsl-script', plugin_dir_url( __FILE__ ).'js/wpsl-scripts.js', array( 'jquery' ), '', true );

	wp_localize_script( 'wpsl-script', 'wp_ajax_params', array(
        	'wpsl_ajax_nonce' => wp_create_nonce( 'wpsl_ajax_nonce' ),
        	'wpsl_ajax_url' => admin_url( 'admin-ajax.php' ),
    	)
  	);

}
add_filter( 'wp_enqueue_scripts', 'wpsl_enqueue_scripts', 100 ,2 );


function wpsl_body_init_functnon(){
	echo '<div class="wpsl-container"></div>';
}
add_action('wp_footer','wpsl_body_init_functnon');

// retrieves the attachment ID from the file URL
function wpsl_pippin_get_image_id($image_url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}

function wpsl_ajax_init_function(){

	// Verify nonce
  	if( !isset( $_POST['wpsl_ajax_nonce'] ) || !wp_verify_nonce( $_POST['wpsl_ajax_nonce'], 'wpsl_ajax_nonce' ) ){
    	die('Permission denied');
  	}
	
	$geturl = sanitize_text_field( $_POST['geturl'] );

	// store the image ID in a var
	$image_id = wpsl_pippin_get_image_id($geturl);

    $img_meta       = wp_prepare_attachment_for_js($image_id);
    $image_title    = $img_meta['title'] == '' ? esc_html_e('Missing title','{domain}') : $img_meta['title'];
    $image_alt      = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];
    $image_desc      = $img_meta['description'];
    $image_src      = wp_get_attachment_image_src($image_id, 'blog-huge', false);
	//print_r($img_meta);
	// retrieve the thumbnail size of our image
	$image_thumb = wp_get_attachment_image_src($image_id, 'full');


	ob_start(); ?>

	<div id="slb_viewer_wrap">
	    <div id="slb_viewer_slb_default" class="slb_viewer slb_theme_slb_default slb_theme_slb_baseline item_single" style="display: block;">
	        <div class="slb_viewer_layout" style="display: block;">
	            <div class="slb_container">
	                <div class="slb_content" style=""><span class="slb_template_tag slb_template_tag_item slb_template_tag_item_content" style="display: inline;"><img src="<?php echo $image_thumb[0]; ?>"></span>
	                    <div class="slb_nav"><span class="slb_prev"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_prev" style="display: block;">Previous</span></span><span class="slb_next"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_next" style="display: block;">Next</span></span>
	                    </div>
	                    <div class="slb_controls"><span class="slb_close"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_close" style="display: block;">Close</span></span><span class="slb_slideshow"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_slideshow_control" style="display: block;">Start slideshow</span></span>
	                    </div>
	                    <div class="slb_loading" style="display: none;"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_loading" style="display: inline;">Loading</span></div>
	                </div>
	                <div class="slb_details" style="">
	                    <div class="inner">
	                        <div class="slb_data">
	                            <div class="slb_data_content">
	                            	<span class="slb_data_title">
	                            		<span class="slb_template_tag slb_template_tag_item slb_template_tag_item_title">
	                            			<?php echo $image_title; ?>
	                            		</span>
	                            	</span>
	                            	<span class="slb_group_status">
	                            		<span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_group_status"></span>
	                            	</span>
	                                <div class="slb_data_desc">
	                                	<span class="slb_template_tag slb_template_tag_item slb_template_tag_item_description">
	                                		<ul>
	                                			<?php echo $image_desc; ?>
	                                		</ul>                                		
	                                	</span>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="slb_nav"><span class="slb_prev"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_prev">Previous</span></span><span class="slb_next"><span class="slb_template_tag slb_template_tag_ui slb_template_tag_ui_nav_next">Next</span></span>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="slb_viewer_overlay" style="display: block; opacity: 0.8;"></div>
	    </div>
	</div>

	<?php echo ob_get_clean();

	wp_die();

}
add_action('wp_ajax_wpsl_ajax_action','wpsl_ajax_init_function');
add_action('wp_ajax_noriv_wpsl_ajax_action','wpsl_ajax_init_function');

